package com.binus.tugas1;

import java.util.Scanner;

/**
 *
 * @author rijal
 */
public class Tugas1 {

    public static void main(String[] args) {
        int jml_barang;
        String nama, nama_barang ;
        Double hrg_beli, hrg_jual;
        
        Scanner masukan = new Scanner(System.in);
        
        System.out.print("Masukan Nama Anda : ");
        nama = masukan.nextLine();
        
        System.out.println("Stock Gudang, Inputkan Barang");
        System.out.println("=============================");
        System.out.println("Selamat Datang " + nama);
        System.out.print("Nama Barang : ");
        nama_barang=masukan.nextLine();
        System.out.print("Jumlah Barang : ");
        jml_barang = masukan.nextInt();
        System.out.print("Harga Beli : ");
        hrg_beli = masukan.nextDouble();
        System.out.print("Harga Jual : ");
        hrg_jual = masukan.nextDouble();
        
        
        System.out.println("Stock Gudang terkait Rinciannya ");
        System.out.println("================================");
        System.out.println("Nama Barang : " + nama_barang);
        System.out.println("Jumlah Barang : " + jml_barang);
        System.out.printf("Harga Beli : Rp. " + "%.2f\n", hrg_beli);
        System.out.printf("Harga Beli : Rp. " + "%.2f\n", hrg_jual);
        
    }
}
